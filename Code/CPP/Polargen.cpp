#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iostream>

// létrehozzuk a Polargen osztályt
class PolarGen
{
public:
    // a konstruktora minden egyes példányosításnál le fog futni
    PolarGen()
    {
        // az első attribútum default értéke legyen true
        nincsTarolt = true;
        // generálunk egy random számot
        std::srand(std::time(NULL));
    }
    // dekonstruktor végzi a memória felszabadítását
    ~PolarGen() {}
    // egyetlen tagfüggvénye az osztályunknak
    double kovetkezo();

private:
    bool nincsTarolt;
    double tarolt;
};

// kovetkezo() függvény működése
double
PolarGen::kovetkezo()
{
    /*
    ha a nincsTarolt attríbútum true akkor előállítjuk a tárolandó számot
    random számokból és matematikából ami nem programozás tehát ugorjuk
    */
    if (nincsTarolt)
    {
        double u1, u2, v1, v2, w;
        do
        {
            u1 = std::rand() / (RAND_MAX + 1.0);
            u2 = std::rand() / (RAND_MAX + 1.0);
            v1 = 2 * u1 - 1;
            v2 = 2 * u2 - 1;
            w = v1 * v1 + v2 * v2;
        } while (w > 1);

        double r = std::sqrt((-2 * std::log(w)) / w);

        // eltároljuk az r*v2 számot, így a következő iterációban ezt
        // fogjuk kiíratni a console-ra
        tarolt = r * v2;
        // false-ra állítjuk a bool attribútumot
        nincsTarolt = !nincsTarolt;

        // visszaadjuk az r * v1 számot
        return r * v1;
    }
    // ha a nincsTárolt attríbútum false, tehát van tárolt szám,
    // visszaadjuk a tárolt számot és true-ra állítjuk a nincsTárolt változót
    else
    {
        nincsTarolt = !nincsTarolt;
        return tarolt;
    }
}
int main(int argc, char **argv)
{
    // létrehozunk egy objektumot ami a Polargen osztálytól örökli a tulajdonságait
    PolarGen pg;

    // legyártunk 10 számot a kovetkezo függvény matekja szerint
    for (int i = 0; i < 10; ++i)
        std::cout << pg.kovetkezo() << std::endl;

    return 0;
}