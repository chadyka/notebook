# ES6-szerű szintaxissal importáljuk
# a tensorflow-t mint tf "változó"
import tensorflow as tf

# Betöltjük az adatokat a példaprogramunkhoz
# az adatokat a tensorflow csomag adja mnist dataset-ből
mnist = tf.keras.datasets.mnist
# A bekért adatokat saját struktúránkra húzzuk rá
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# Megadjuk a neurális háló modelljének iniciális
# alakját és definiáljuk a layereket amiket
# majd használni fogunk
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

# numpy segítségével megjelenítjük az adatokat
prediction = model(x_train[:1]).numpy()
prediction

# kiszámoljuk a softmax aktivációkat,
# majd numpy segítségével megjelenítjük
tf.nn.softmax(prediction).numpy()

# létrehozunk egy változót amiben az elveszett adatok lesznek tárolva
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

# kiíratjuk a veszteséget is
loss_fn(y_train[:1],prediction).numpy()

# összerakjuk a modellünket "adam" optimalizációs
# stratégiával, loss_fn veszteségekkel és a
# pontosság mértékét egybefűzve
model.compile(optimizer='adam',
    loss=loss_fn,
    metrics=['accuracy'])

# a modellünket illesztjük a korábban megadott
# objektumunkra
model.fit(x_train, y_train, epochs=5)

# "kiszámoljuk" a modell-t 2-es szintű
# bőbeszédességgel
model.evaluate(x_test, y_test, verbose=2)

# átalakítjuk az outputot, innentől csak
# valószínűséget fogunk majd látni a kimeneten
probbility_model = tf.keras.Sequential([
    model,
    tf.keras.layers.Softmax()
])
probbility_model(x_test[:5])